class Person{
    constructor(name,age,salary,sex)
    {
        this.name=name;
        this.age=age;
        this.salary=salary;
        this.sex=sex;
    }
    
    static sort(arr,val,method)
        {
        let arr1=[...arr];
        let quickSort=(start,end)=>
        {
            if (start >= end) {
                return;
            }
            
            // Returns pivotIndex
            // let index = partition(arr1, start, end);
            const pivotValue = arr1[end];
            let pivotIndex = start; 
            for (let i = start; i < end; i++) {
            if (arr1[i].name < pivotValue.name) {
            // Swapping elements
            [arr1[i], arr1[pivotIndex]] = [arr1[pivotIndex], arr1[i]];
            // Moving to next element
            pivotIndex++;
            }
        
        
        // Putting the pivot value in the middle
            [arr1[pivotIndex], arr1[end]] = [arr1[end], arr1[pivotIndex]] 
            // Recursively apply the same logic to the left and right subarrays
            // arr1[end] = arr1[index];
            // sortedArray[ndex] = pivotValue;
            quickSort( start, index - 1);
            quickSort(index + 1, end)

        }
        quickSort(0,arr1.length-1);
        if(method=="desc")
            return arr1.reverse();
        else
            return arr1;
    }
}

// function partition(arr, start, end){
//     // Taking the last element as the pivot
//     const pivotValue = arr[end];
//     let pivotIndex = start; 
//     for (let i = start; i < end; i++) {
//         if (arr[i].name < pivotValue.name) {
//         // Swapping elements
//         [arr[i], arr[pivotIndex]] = [arr[pivotIndex], arr[i]];
//         // Moving to next element
//         pivotIndex++;
//         }
//     }
    
//     // Putting the pivot value in the middle
//     [arr[pivotIndex], arr[end]] = [arr[end], arr[pivotIndex]] 
//     return pivotIndex;
// };
// static sort(arr,val,method)
// {
//     let arr1=[...arr];
//     let quickSort=(start,end)=>
//     {
//         if (start >= end) {
//             return;
//         }
        
//         // Returns pivotIndex
//         // let index = partition(arr1, start, end);
//         const pivotValue = arr1[end];
//         let pivotIndex = start; 
//         for (let i = start; i < end; i++) {
//         if (arr1[i].name < pivotValue.name) {
//         // Swapping elements
//         [arr1[i], arr1[pivotIndex]] = [arr1[pivotIndex], arr1[i]];
//         // Moving to next element
//         pivotIndex++;
//         }
    
    
//     // Putting the pivot value in the middle
//         [arr1[pivotIndex], arr1[end]] = [arr1[end], arr1[pivotIndex]] 
//         // Recursively apply the same logic to the left and right subarrays
//         // arr1[end] = arr1[index];
//         // sortedArray[ndex] = pivotValue;
//         quickSort( start, index - 1);
//         quickSort(index + 1, end)

//     };
//     quickSort(0,arr1.length-1);
//     if(method=="desc")
//         return arr1.reverse();
//     else
//         return arr1;
// };

p1=new Person("Rahul",28,10000,"M");
 p2=new Person("Rohit",30,40000,"M");
  p3=new Person("Kunal",30,50000,"M");
 p4=new Person("Lakshya",24,50000,"M");
 p5=new Person("Tarun",50,70000,"M");

console.log(p1);
var arr=[p1,p2,p3,p4,p5];

console.log(Person.sort(arr,"name","desc"));

