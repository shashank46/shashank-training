// const onButtonClick = (status)=>{
//     if(status == 1){
//       sendLog('processing')
//       jumpTo('IndexPage')
//     }else if(status == 2){
//       sendLog('fail')
//       jumpTo('FailPage')
//     }else if(status == 3){
//       sendLog('fail')
//       jumpTo('FailPage')
//     }else if(status == 4){
//       sendLog('success')
//       jumpTo('SuccessPage')
//     }else if(status == 5){
//       sendLog('cancel')
//       jumpTo('CancelPage')
//     }else {
//       sendLog('other')
//       jumpTo('Index')
//     }
//   }

//   const onButtonClick = (status)=>{
//     switch (status){
//       case 1:
//         sendLog('processing')
//         jumpTo('IndexPage')
//         break
//       case 2:
//       case 3:
//         sendLog('fail')
//         jumpTo('FailPage')
//         break  
//       case 4:
//         sendLog('success')
//         jumpTo('SuccessPage')
//         break
//       case 5:
//         sendLog('cancel')
//         jumpTo('CancelPage')
//         break
//       default:
//         sendLog('other')
//         jumpTo('Index')
//         break
//     }
//   }

//   const actions = {
//     '1': ['processing','IndexPage'],
//     '2': ['fail','FailPage'],
//     '3': ['fail','FailPage'],
//     '4': ['success','SuccessPage'],
//     '5': ['cancel','CancelPage'],
//     'default': ['other','Index'],
//   }
  
//   const onButtonClick = (status)=>{
//     let action = actions[status] || actions['default'],
//         logName = action[0],
//         pageName = action[1]
//     sendLog(logName)
//     jumpTo(pageName)
//   }

// const actions = new Map([
//     [1, ['processing','IndexPage']],
//     [2, ['fail','FailPage']],
//     [3, ['fail','FailPage']],
//     [4, ['success','SuccessPage']],
//     [5, ['cancel','CancelPage']],
//     ['default', ['other','Index']]
//   ])
  
//   const onButtonClick = (status)=>{
//     let action = actions.get(status) || actions.get('default')
//     sendLog(action[0])
//     jumpTo(action[1])
//   }


// const onButtonClick = (status,identity)=>{
//     if(identity == 'guest'){
//       if(status == 1){
//         //do sth
//       }else if(status == 2){
//         //do sth
//       }else if(status == 3){
//         //do sth
//       }else if(status == 4){
//         //do sth
//       }else if(status == 5){
//         //do sth
//       }else {
//         //do sth
//       }
//     }else if(identity == 'master') {
//       if(status == 1){
//         //do sth
//       }else if(status == 2){
//         //do sth
//       }else if(status == 3){
//         //do sth
//       }else if(status == 4){
//         //do sth
//       }else if(status == 5){
//         //do sth
//       }else {
//         //do sth
//       }
//     }
//   }

  const actions = ()=>{
    const functionA = ()=>{/*do sth*/}
    const functionB = ()=>{/*do sth*/}
    const functionC = ()=>{/*send log*/}
    return new Map([
      [/^guest_[1-4]$/,functionA],
      [/^guest_5$/,functionB],
      [/^guest_.*$/,functionC],
      //...
    ])
  }
  
  const onButtonClick = (identity,status)=>{
    let action = [...actions()].filter(([key,value])=>(key.test(`${identity}_${status}`)))
    action.forEach(([key,value])=>value.call(this))
  }

