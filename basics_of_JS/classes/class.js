// class Car {
//     constructor(name, year) {
//       this.name = name;
//       this.year = year;
//     }
//     age(x) {
//       return x - this.year;
//     }
//   }
  
//   let date = new Date();
//   let year = date.getFullYear();
  
//   let myCar = new Car("Ford", 2014);
//   document.getElementById("demo").innerHTML=
//   "My car is " + myCar.age(year) + " years old.";

class Animal {
    constructor(name) {
      this.name = name;
    }
  
    speak() {
      console.log(`${this.name} makes a noise.`);
    }
  }
  
  class Dog extends Animal {
    constructor(name) {
      super(name); // call the super class constructor and pass in the name parameter
    }
  
    speak() {
      console.log(`${this.name} barks.`);
    }
  }
  
  let d = new Dog('Mitzie');
  d.speak(); // Mitzie barks.