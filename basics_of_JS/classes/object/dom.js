// document
// #document
// document.getElementById("one");
// <h1 id=​"one">​Heading1​</h1>​
// var heading=document.getElementById("one");
// undefined
// heading.style.color="red";
// "red"
// heading.style.backgroundColor="cyan";
// "cyan"
// var headings=document.getElementsByTagName('h1');
// undefined
// heading
// <h1 id=​"one" style=​"color:​ red;​ background-color:​ cyan;​">​Heading1​</h1>​
// headings
// HTMLCollection(2) [h1#one, h1#two, one: h1#one, two: h1#two]
// headings[1].innerText="second heading";
// "second heading"